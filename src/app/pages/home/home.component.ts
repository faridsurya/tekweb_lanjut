import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ApiService} '../../api.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(config: NgbModalConfig, private modalService: NgbModal, private api:ApiService) { }

  ngOnInit() {
    this.getData();
  }

  dataSource:any=[];
  user:any={};

  getData()
  {
    
      this.api.find().subscribe(data=>{
        this.dataSource=data;
      })
      

      
  }

  modalTitle:any;
  open(content,data) {
    this.modalTitle='Membuat Data Baru';
    this.modalService.open(content);
  }

  edit(content,data)
  {
      this.modalTitle='Memperbaharui Data';
     this.user={
        name:data.employee_name,
        age:data.employee_age,
        salary:data.employee_salary,
        id:data.id
        };
    this.modalService.open(content);

  }

  loading:boolean;
  saveData(user)
  {
    this.loading=true;
    //buat baru jika id tidak terdefinisi, update jika id exist.
    if(user.id==undefined)
    {
      this.api.create(user).subscribe(data=>{
          this.loading=false;
          this.pushData(data);
          this.user={};

      })
    }else{
      var dt={
        name:user.name,
        salary:user.salary,
        age:user.age
      };
      this.api.update(user.id,dt).subscribe(data=>{
           this.loading=false;
         this.user={};
         alert('Data berhasil diperbaharui');
      })
    }
  }

  loadingDel:any={};
  delete(id,idx)
  {
    var conf=confirm('Hapus Data?');
    if(conf)
    {
      this.loadingDel[id]=true;
      this.api.delete(id).subscribe(data=>{
          this.loadingDel[id]=false;
          this.dataSource.splice(idx,1);
      })
    }
  }
  pushData(data)
  {
      var dat={
        employee_name:data.name,
        employee_age:data.age,
        employee_salary:data.salary
        id:data.id
      };
      this.dataSource.unshift(dat);
      alert('Data berhasil disimpan');
  }

  deleteData(idx)
  {
    var konfirmasi=confirm('Yakin akan menghapus data?');
    if(konfirmasi==true)
    {
      this.dataSource.splice(idx,1);
    }
  }


}
