import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
  	private http:HttpClient
  ) { }

  serverUrl:any='http://dummy.restapiexample.com/api/v1';
  
  find()
  {
  		return this.http.get(this.serverUrl+'/employees');
  }
  findone(id)
  {
  		return this.http.post(this.serverUrl+'/employee/'+id);
  }
  create(data)
  {
  		return this.http.post(this.serverUrl+'/create',data);
  }
  update(id,data)
  {
  		return this.http.put(this.serverUrl+'/update/'+id,data);
  }
  delete(id)
  {
  		return this.http.delete(this.serverUrl+'/delete/'+id);
  }
  test()
  {
  	return 'OK';
  }
}
